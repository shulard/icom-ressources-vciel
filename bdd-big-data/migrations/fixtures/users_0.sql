INSERT INTO users (uid, email, firstname, lastname)
VALUES
    ('118b8ba2-8f29-4ac0-9e3f-08b06aa211c8', 'super@email.com', 'John', 'Doe'),
    ('8f4ffd69-3167-40f4-8cd4-843a84b1405f', 'super@email.com', 'Jane', 'Doe'),
    ('5d77706c-daa4-4c0a-81e3-f0443e85fdf0', 'autre@email.com', 'Bart', 'Simpson'),
    ('172c5378-8ded-460c-9de6-34d8d6e6f6b9', 'lisa@simps.on', 'Lisa', 'Simpson');
