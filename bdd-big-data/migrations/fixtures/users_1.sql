INSERT INTO users (uid, email, firstname, lastname, created_at)
VALUES
    ('118b8ba2-8f29-4ac0-9e3f-08b06aa211c8', 'super@email.com', 'John', 'Doe', '2021-10-31 10:00:01'),
    ('8f4ffd69-3167-40f4-8cd4-843a84b1405f', 'super@email.com', 'Jane', 'Doe', '2021-10-18 11:00:11'),
    ('5d77706c-daa4-4c0a-81e3-f0443e85fdf0', 'autre@email.com', 'Bart', 'Simpson', '2021-10-12 12:00:00'),
    ('172c5378-8ded-460c-9de6-34d8d6e6f6b9', 'lisa@simps.on', 'Lisa', 'Simpson', '2021-10-30 14:57:46');
