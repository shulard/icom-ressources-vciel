ALTER TABLE users ADD firstname VARCHAR(255), lastname VARCHAR(255);
UPDATE users SET
    firstname = substr(full_name, 1, instr(full_name,' ')),
    lastname = substr(full_name, instr(full_name,' '));
ALTER TABLE users DROP full_name;
