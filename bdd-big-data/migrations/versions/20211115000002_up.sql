ALTER TABLE users ADD full_name VARCHAR(255);
UPDATE users SET full_name = firstname || ' ' || lastname;
ALTER TABLE users DROP firstname;
ALTER TABLE users DROP lastname;
